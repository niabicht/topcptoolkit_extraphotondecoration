A comprehensive documentation is being built and will appear [here](https://topcptoolkit.docs.cern.ch/).

In the meantime, you can run the code on a `DAOD_PHYS` sample as:
```bash
runTop_el.py -i input_sample.txt -o outputDir --parton --particle
```