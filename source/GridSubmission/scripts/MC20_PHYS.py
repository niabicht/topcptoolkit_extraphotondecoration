import GridSubmission.grid

GridSubmission.grid.Add('PHYS_ttbar_PP8_mc20e').datasets = [
    'mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3681_r13145_p5855'
]
