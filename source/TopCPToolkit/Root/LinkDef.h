#include "TopPartons/PartonHistory.h"

#ifdef __CINT__
#pragma extra_include "TopPartons/PartonHistory.h";

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class xAOD::PartonHistory+;
#pragma link C++ class xAOD::PartonHistoryAux+;
#pragma link C++ class xAOD::PartonHistoryContainer+;
#pragma link C++ class xAOD::PartonHistoryAuxContainer+;

#endif
